<?php
echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
<div class="container">
    <h1>Заполните форму:</h1>
    <form action="'.$_SERVER['SCRIPT_NAME'].'" method="POST">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Email:</label>
                <input type="email" class="form-control" id="inputEmail4" name="email" placeholder="Email">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Пароль:</label>
                <input type="password" class="form-control" id="inputPassword4" name="password" placeholder="Пароль">
            </div>
        </div>
        <div class="form-group">
            <label for="inputAddress">Способ доставки:</label>
            <input type="text" class="form-control" id="inputAddress" name="delivery" placeholder="Самовывоз">
        </div>
        <div class="form-group">
            <label for="inputAddress2">Номер заказа:</label>
            <input type="text" class="form-control" id="inputAddress2" name="number_order" placeholder="Номер заказа">
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity">Город</label>
                <input type="text" class="form-control" id="inputCity" name="user_city">
            </div>
            <div class="form-group col-md-4">
                <label for="inputState">Отделение почты:</label>
                <select id="inputState" class="form-control" name="branch">
                    <option selected>Выберите...</option>
                    <option>Отделение №1</option>
                    <option>Отделение №2</option>
                    <option>Отделение №3</option>
                    <option>Отделение №4</option>
                </select>
            </div>

        </div>
        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-label" for="gridCheck">
                    Check me out
                </label>
                <br>
                <label for="points">Оцените нашу работу от 0 до 10:</label>
                <input type="range"
                       id="points" name="points" min="0" max="10">
            </div>
        </div>
        <button type="submit" class="btn btn-success" name="send_order" id="button_send">Отправить</button>
    </form>';


    $user_email = $_REQUEST['email'];
    $user_password = $_REQUEST['password'];
    $user_delivery = $_REQUEST['delivery'];
    $user_number_order = $_REQUEST['number_order'];
    $user_city = $_REQUEST['user_city'];
    $user_branch = $_REQUEST['branch'];
    $user_points = $_REQUEST['points'];
if (isset($_REQUEST['send_order'])) {
        echo '<div class=output>';
        echo '<span><strong>Ваша заявка прошла успешно!</strong></span>';
        echo '<p>Email:  '.$user_email.'</p>';
        echo '<p>Способ доставки:  '.$user_delivery.' </p>';
        echo '<p>Номер заказа:  '.$user_number_order.' </p>';
        echo '<p>Город:  '.$user_city.' </p>';
        echo '<p>Отделение:   '.$user_branch .'</p>';
        echo '<p>Ваша оценка: '. $user_points .' </p>';
        echo '</div>';
}

echo '</div>';
