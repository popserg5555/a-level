<?php
//Написать программу, которая выводит простые числа, т.е. делящиеся без
//остатка только на себя и на 1.

// for ------------------------------------------------------------------------------------
for ($i = 1; $i <= 100; $i++) {
    $isSimple = true;
    for ($k = $i - 1; $k > 1; $k--) {
        if ($i % $k == 0) {
            $isSimple = false;
            break;
        }
    }
    if ($isSimple) {
        echo $i . "\n";
    }
}
echo '<br>';
// while ------------------------------------------------------------------------------------
$i = 1;
while ($i <= 100) {
    $isSimple = true;
    $k = $i - 1;
    while ($k > 1) {
        if ($i % $k == 0) {
            $isSimple = false;
            break;
        }
        $k--;
    }
    if ($isSimple) {
        echo $i . "\n";
    }
    $i++;
}

echo '<br>';
// do while ------------------------------------------------------------------------------------
$i = 1;
do {
    $isSimple = true;
    $k = $i - 1;
    do {
        if ($k != 0 && $i % $k == 0) {
            $isSimple = false;
            break;
        }
        $k--;
    } while ($k > 1);
    if ($isSimple) {
        echo $i . "\n";
    }
    $i++;
} while ($i <= 100);
echo '<br>';

//2) Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.

// while ------------------------------------------------------------------------------------
$count = 0;
$x = 1;
while ($x++ <= 100) {
    $number = rand(1, 100);
    if ($number % 2 == 0) {
        echo $number . ' ';
        $count++;
    }
}
echo '<br>';
echo $count;
echo '<br>';
// for --------------------------------------------
$count = 0;
for ($x = 1; $x <= 100; $x++) {
    $number = rand(1, 100);
    if ($number % 2 == 0) {
        echo $number . ' ';
        $count++;
    }
}
echo '<br>';
echo $count;
echo '<br>';
// do while ------------------------------------------------------------------------------------
$count = 0;
$x = 1;
do {
    $number = rand(1, 100);
    if ($number % 2 == 0) {
        echo $number . ' ';
        $count++;
    }
} while ($x++ <= 100);
echo '<br>';
echo $count;
echo '<br>';

//3) Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).

$a = [];
$b = [1, 2, 3, 4, 5];
for ($i = 1; $i <= 100; $i++) {
    $a[] = rand(1, 5);
}

foreach ($b as $t) {
    $counter = 0;
    foreach ($a as $g) {
        if ($t == $g) {
            $counter++;
        }
    }
    echo $t . ' - ' . $counter;
    echo '<br>';
}


// while ------------------------------------------------------------------------------------
echo '<br>';
$i = 1;
$a = [];
$b = [1, 2, 3, 4, 5];
while ($i <= 100) {
    $a[] = rand(1, 5);
    $i++;
}
$x = 0;
while ($x < 5) {
    $counter = 0;
    $y = 0;
    while ($y < 100) {
        if ($b[$x] == $a[$y]) {
            $counter++;
        }
        $y++;
    }
    echo $b[$x] . ' - ' . $counter;
    echo '<br>';
    $x++;

}

echo '<br>';
// do while ------------------------------------------------------------------------------------
$i = 1;
$a = [];
$b = [1, 2, 3, 4, 5];
do {
    $a[] = rand(1, 5);
    $i++;
} while ($i <= 100);
$x = 0;
do {
    $counter = 0;
    $y = 0;
    do {
        if ($b[$x] == $a[$y]) {
            $counter++;
        }
        $y++;
    } while ($y < 100);
    echo $b[$x] . ' - ' . $counter;
    echo '<br>';
    $x++;
} while ($x < 5);


// for--------------------------------------------------------
echo '<br>';
$i = 1;
$a = [];
$b = [1, 2, 3, 4, 5];
for ($i = 1; $i <= 100; $i++) {
    $a[] = rand(1, 5);
}

for ($x = 0; $x < 5; $x++) {
    $counter = 0;

    for ($y = 0; $y < 100; $y++) {
        if ($b[$x] == $a[$y]) {
            $counter++;
        }
    }
    echo $b[$x] . ' - ' . $counter;
    echo '<br>';

}

//4) Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.

// for ------------------------------------------------------------------------------------
$r = 1;
$g = 255;
$b = 1;

echo "<table border='1'>";
for ($a = 0; $a < 3; $a++) {
    echo '<tr>';
    for ($d = 0; $d < 5; $d++) {
        $r += 17;
        $g -= 17;
        $b += 17;
        echo '<td style=background-color:rgb('.$r.','. $g.','. $b.')>&nbps</td>';
    }
}
echo '</tr>';
echo '</table>';
echo '<br>';
//while  --------------------------
$r = 1;
$g = 255;
$b = 1;

echo "<table border='1'>";
$a = 0;
while ($a < 3) {
    echo '<tr>';
    $d = 0;
    while ($d < 5) {
        $r += 17;
        $g -= 17;
        $b += 17;
        echo '<td style=background-color:rgb('.$r.','. $g.','. $b.')>&nbps</td>';
        $d++;
    }
    $a++;
}
echo '</tr>';
echo '</table>';
echo '<br>';

//- do while ---------------------------------------------
$r = 1;
$g = 255;
$b = 1;

echo '<table border=1>';
$a = 0;
do {
    echo '<tr>';
    $d = 0;
    do {
        $r += 17;
        $g -= 17;
        $b += 17;
        echo '<td style=background-color:rgb('.$r.','. $g.','. $b.')>&nbps</td>';
        $d++;
    } while ($d < 5);
    $a++;

} while ($a < 3);

echo '</tr>';
echo '</table>';
echo '<br>';



//В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).


$month = rand(1, 12);
if ($month == 1 || $month == 2 || $month == 12) {
    echo $month . ' Зима';
} elseif ($month == 3 || $month == 4 || $month == 5) {
    echo $month . ' Весна';
} elseif ($month == 6 || $month == 7 || $month == 8) {
    echo $month . ' Лето';
} else {
    echo $month . ' Осень';
}

//Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква 'a'. Если это так - выведите 'да', в противном случае выведите 'нет'.

$a = 'abcde';
echo $a . '<br>';

if ($a[0] == 'a') {
    echo 'Yes';
} else {
    echo 'No';
}
//Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. Если это так - выведите 'да', в противном случае выведите 'нет'.

$i = '12345';
echo $i . '<br>';
if ($i[0] == 1 || $i[0] == 2 || $i[0] == 3) {
    echo 'Первая цифра строки 1,2 или 3' . '<br>';
} else {
    echo 'Первая цифра строки не 1,2 или 3' . '<br>';
}
//Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. Напишите два варианта скрипта - тернарка и if else.

$test = false;
if ($test) {
    echo 'Верно' . '<br>';
} else {
    echo 'Неверно' . '<br>';
}
echo '<br>';

$test = true;
echo $test ? 'Верно' : 'Неверно';

//Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
//Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. Сделат через if else и через тернарку.

$rus = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
$eng = ['Monday', 'Tuesday', 'Wednesday', 'Thusday', 'Friday', 'Saturday', 'Sunday'];
$lang = [];
if (isset($_REQUEST['lang'])) {
    if ($_REQUEST['lang'] == 'RUSSIAN') {
        echo '<pre>';
        // phpcs:disable
        print_r($rus);
        // phpcs:enable
        echo '</pre>';
    } elseif ($_REQUEST['lang'] == 'ENGLAND') {
        echo '<pre>';
        // phpcs:disable
        print_r($eng);
        // phpcs:enable
        echo '</pre>';
    }
    // phpcs:disable
    $_REQUEST['lang'] == 'RUSSIAN' ? print_r($rus) : print_r($eng);
    // phpcs:enable
}

echo '<form action="'.$_SERVER['SCRIPT_NAME'].'" method="post">
    <input type="submit" name="lang" value="RUSSIAN">
    <input type="submit" name="lang" value="ENGLAND">
</form>';



//В переменной cloсk лежит число от 0 до 59 – это минуты. Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую). тернарка и if else.-->

$clock = rand(1, 59);
echo $clock . '<br>';
if ($clock <= 14) {
    echo '1/4 часа';
} else if ($clock >= 15 && $clock <= 29) {
    echo '2/4 часа';
} else if ($clock >= 30 && $clock <= 44) {
    echo '3/4 часа';
} else {
    echo '4/4 часа';
}

echo '<br>';
$clock = rand(1, 59);
echo $clock . '<br>';
$result = ($clock <= 14) ? '1/4 часа' : '';
$result1 = ($clock >= 15 && $clock <= 29) ? '2/4 часа' : '';
$result2 = ($clock >= 30 && $clock <= 44) ? '3/4 часа' : '';
$result3 = ($clock >= 45) ? '4/4 часа' : '';
echo $result . $result1 . $result2 . $result3;


//1. Дан массив-->
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//Развернуть этот массив в обратном направлении



$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$result = [];
$names_count = 0;
foreach ($names as $name) {
    $names_count++;
}
// for ------------------------------------------------------------------------------------
echo $names_count;
for ($x = $names_count - 1; $x >= 0; $x--) {
    $result[] = $names[$x];
}

echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';
// while ------------------------------------------------------------------------------------
$x = $names_count - 1;
$result = [];
while ($x >= 0) {
    $result[] = $names[$x];
    $x--;
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';
// do while ------------------------------------------------------------------------------------
$x = $names_count - 1;
$result = [];
do {
    $result[] = $names[$x];
    $x--;
} while ($x >= 0);
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';
// foreach ------------------------------------------------------------------------------------
$x = $names_count - 1;
$result = [];
foreach ($names as $name) {
    $result[] = $names[$x];
    $x--;
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

//Дан массив
//[44, 12, 11, 7, 1, 99, 43, 5, 69]
//Развернуть этот массив в обратном направлении

$names = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$result = [];
$names_count = 0;
foreach ($names as $name) {
    $names_count++;
}
echo $names_count;
// for ------------------------------------------------------------------------------------
for ($x = $names_count - 1; $x >= 0; $x--) {
    $result[] = $names[$x];
}

echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';
// while ------------------------------------------------------------------------------------
$x = $names_count - 1;
$result = [];
while ($x >= 0) {
    $result[] = $names[$x];
    $x--;
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';
// do while ------------------------------------------------------------------------------------
$x = $names_count - 1;
$result = [];
do {
    $result[] = $names[$x];
    $x--;
} while ($x >= 0);
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';
// foreach ------------------------------------------------------------------------------------
$x = $names_count - 1;
$result = [];
foreach ($names as $name) {
    $result[] = $names[$x];
    $x--;
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

//Дана строка
//let str = 'Hi I am ALex'
//развенуть строку в обратном направлении.

$string = 'Hi I am ALex';

$strarray = [];
$x = 0;
while (isset($string[$x])) {
    $strarray[] = $string[$x];
    $x++;
}

$result = [];
$str_count = 0;
foreach ($strarray as $value) {
    $str_count++;
}
// for ------------------------------------------------------------------------------------
for ($x = $str_count - 1; $x >= 0; $x--) {
    $result[] = $strarray[$x];
}
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';
// while ------------------------------------------------------------------------------------
$x = $str_count - 1;
$result = [];
while ($x >= 0) {
    $result[] = $strarray[$x];
    $x--;
}
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';

// do while ------------------------------------------------------------------------------------
$x = $str_count - 1;
$result = [];
do {
    $result[] = $strarray[$x];
    $x--;
} while ($x >= 0);
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';
// foreach ------------------------------------------------------------------------------------
$x = $str_count - 1;
$result_str = '';
foreach ($strarray as $value) {
    $result_str .= $strarray[$x];
    $x--;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';



//Дана строка. готовую функцию toUpperCase() or tolowercase()
//let str = 'Hi I am ALex'
//сделать ее с с маленьких букв


$string = 'Hi I am ALex';
$strarray = [];
$x = 0;
while (isset($string[$x])) {
    $strarray[] = $string[$x];
    $x++;
}

$result = [];
$str_count = 0;
foreach ($strarray as $value) {
    $str_count++;
}
// for ------------------------------------------------------------------------------------
for ($x = 0; $x < $str_count; $x++) {
    $result[] = strtolower($strarray[$x]);
}
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';

$x = 0;
$result = [];
// while ------------------------------------------------------------------------------------
while ($x < $str_count) {
    $result[] = strtolower($strarray[$x]);
    $x++;
}
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';

// do while ------------------------------------------------------------------------------------
$x = 0;
$result = [];
do {
    $result[] = strtolower($strarray[$x]);
    $x++;
} while ($x < $str_count);
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';

// foreach ------------------------------------------------------------------------------------
$result_str = '';
foreach ($strarray as $value) {
    $result_str .= strtolower($value);
}

echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';


//Дана строка
//let str = 'Hi I am ALex'
//сделать все буквы большие



$string = 'Hi I am ALex';
$strarray = [];
$x = 0;
while (isset($string[$x])) {
    $strarray[] = $string[$x];
    $x++;
}

$result = [];
$str_count = 0;
foreach ($strarray as $value) {
    $str_count++;
}
// for ------------------------------------------------------------------------------------
for ($x = 0; $x < $str_count; $x++) {
    $result[] = strtoupper($strarray[$x]);
}
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';
// while  ------------------------------------------------------------------------------------
$x = 0;
$result = [];

while ($x < $str_count) {
    $result[] = strtoupper($strarray[$x]);
    $x++;
}
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';

// do while ------------------------------------------------------------------------------------
$x = 0;
$result = [];
do {
    $result[] = strtoupper($strarray[$x]);
    $x++;
} while ($x < $str_count);
$result_str = '';
foreach ($result as $value) {
    $result_str .= $value;
}
echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';

// foreach ------------------------------------------------------------------------------------
$result_str = '';
foreach ($strarray as $value) {
    $result_str .= strtoupper($value);
}

echo '<pre>';
// phpcs:disable
print_r($result_str);
// phpcs:enable
echo '</pre>';
echo '<br>';


//Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//сделать все буквы с маленькой

$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$names_count = 0;
foreach ($names as $name) {
    $names_count++;
}
// for -------------------------------------------------------------
$result = [];
for ($x = 0; $x < $names_count; $x++) {
    $result[] = strtolower($names[$x]);
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

// while---------------------------------------------
$result = [];
$x = 0;
while ($x < $names_count) {
    $result[] = strtolower($names[$x]);
    $x++;
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

// do while ---------------------------------------------------------------

$result = [];
$x = 0;
do {
    $result[] = strtolower($names[$x]);
    $x++;
} while ($x < $names_count);

echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

// foreach --------------------------------------------------------------------

$result = [];
foreach ($names as $value) {
    $result[] = strtolower($value);

}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

//Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//сделать все буквы с большой

$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$names_count = 0;
foreach ($names as $name) {
    $names_count++;
}
// for -------------------------------------------------------------
$result = [];
for ($x = 0; $x < $names_count; $x++) {
    $result[] = strtoupper($names[$x]);
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

// while---------------------------------------------
$result = [];
$x = 0;
while ($x < $names_count) {
    $result[] = strtoupper($names[$x]);
    $x++;
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

// do while ---------------------------------------------------------------

$result = [];
$x = 0;
do {
    $result[] = strtoupper($names[$x]);
    $x++;
} while ($x < $names_count);

echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

// foreach --------------------------------------------------------------------

$result = [];
foreach ($names as $value) {
    $result[] = strtoupper($value);

}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

//Дано число
//let num = 1234678
//развернуть ее в обратном направлении

$num = 1234678;
$result_str = '';
$calc = $num;
$counter = 0;
while ($calc > 0) {
    $calc = (int)($calc / 10);
    $counter++;
}

// for --------------------------------------------------------
$calc = $num;
$result = '';
for ($x = 0; $x < $counter; $x++) {
    $modulo = $calc % 10;
    $result .= $modulo;
    $calc = (int)($calc / 10);
}
echo '<pre>';
echo $result;
echo '</pre>';
// while -----------------------------------------
$calc = $num;
$result = '';
$x = 0;
while ($x < $counter) {
    $modulo = $calc % 10;
    $result .= $modulo;
    $calc = (int)($calc / 10);
    $x++;
}

echo '<pre>';
echo $result;
echo '</pre>';

// do while -----------------------------------------------------------
$calc = $num;
$result = '';
$x = 0;
do {
    $modulo = $calc % 10;
    $result .= $modulo;
    $calc = (int)($calc / 10);
    $x++;
} while ($x < $counter);
echo '<pre>';
echo $result;
echo '</pre>';

//foreach--------------------------------------------------------------------
$string_num = (string)($num);
$number_array = [];
for ($i = 0; $i < $counter; $i++) {
    $number_array[] = $string_num[$i];
}
$result = '';
$k = $counter - 1;
foreach ($number_array as $value) {
    $result .= $number_array[$k];
    $k--;
}
echo '<pre>';
echo $result;
echo '</pre>';

//Дан массив
//[44, 12, 11, 7, 1, 99, 43, 5, 69]
//отсортируй его в порядке убывания

$numbers = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$result = [];
foreach ($numbers as $numb) {
    $maxNumber = $numbers[0];
    $maxNumberKey = 0;
    foreach ($numbers as $key => $value) {
        if ($value > $maxNumber) {
            $maxNumber = $value;
            $maxNumberKey = $key;
        }
    }
    $result[] = $numbers[$maxNumberKey];
    unset($numbers[$maxNumberKey]);
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

// for -----------------------------------------------------------
$count_el = 0;
$numbers = [44, 12, 11, 7, 1, 99, 43, 5, 69];

while (isset($numbers[$count_el])) {
    $count_el++;
}

$result = [];
for ($i = 0; $i < $count_el; $i++) {
    if (isset($numbers[$i])) {
        $maxNumber = $numbers[0];
        $maxNumberKey = 0;
        for ($k = 0; $k < $count_el; $k++) {
            if (isset($numbers[$k])) {
                if ($numbers[$k] > $maxNumber) {
                    $maxNumber = $numbers[$k];
                    $maxNumberKey = $k;
                }
            }

        }
        $result[] = $numbers[$maxNumberKey];
        unset($numbers[$maxNumberKey]);
        $i = 0;
    }

}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';


// while--------------------------------------------------
$numbers = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$result = [];
$i = 0;
while ($i < $count_el) {
    if (isset($numbers[$i])) {
        $maxNumber = $numbers[0];
        $maxNumberKey = 0;
        $k = 0;

        while ($k < $count_el) {
            if (isset($numbers[$k])) {
                if ($numbers[$k] > $maxNumber) {
                    $maxNumber = $numbers[$k];
                    $maxNumberKey = $k;
                }
            }
            $k++;
        }
        $result[] = $numbers[$maxNumberKey];
        unset($numbers[$maxNumberKey]);
        $i = 0;
    }
    $i++;
}
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';

//do while----------------------------------------------------------------------

$numbers = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$result = [];
$i = 0;
do {
    if (isset($numbers[$i])) {
        $maxNumber = $numbers[0];
        $maxNumberKey = 0;
        $k = 0;
        do {
            if (isset($numbers[$k])) {
                if ($numbers[$k] > $maxNumber) {
                    $maxNumber = $numbers[$k];
                    $maxNumberKey = $k;
                }
            }
            $k++;
        } while ($k < $count_el);
        $result[] = $numbers[$maxNumberKey];
        unset($numbers[$maxNumberKey]);
        $i = 0;
    }
    $i++;
} while ($i < $count_el);
echo '<pre>';
// phpcs:disable
print_r($result);
// phpcs:enable
echo '</pre>';
