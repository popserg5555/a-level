<?php
if (isset($_REQUEST['Go'])) {
    $number = $_REQUEST['number'];
    if ($number != '') {
        $random_number = rand(5, 8);
        if ($number < 5) {
            echo 'Число маленькое';
        } elseif ($number > 8) {
            echo 'Число большое';
        } elseif ($number == $random_number) {
            echo 'Вы угадали!';
            $button_name = 'Играть еще';
        } else {
            echo 'Проиграли';
            $button_name = 'Попробуйте еще';
        }
    } else {
        echo 'Вы отправили пустое поле!';
    }
}
require_once 'form.php';
