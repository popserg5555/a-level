<?php

interface can_attack
{
    public function set_cannon(Cannon $cannon);
    public function prepare_cannon();
    public function shoot();
}

abstract class Cannon
{
    protected $is_prepare = false;

    public function prepare()
    {

    }

    public function shoot()
    {

    }
}

class Cannonry extends Cannon
{
    public function prepare()
    {
            $this->is_prepare = true;
            return "The cannonary are ready for battle";
    }

    public function shoot()
    {
        if($this->is_prepare) {
            return "Cannonary - shot";
        } else {
            return "Cannonary unprepared";
        }
    }
}

class Air_defense extends Cannon
{
    public function prepare()
    {
        $this->is_prepare = true;
        return "Air defense ready to fire";
    }

    public function shoot()
    {
        if ($this->is_prepare) {
            return "Air defence - shot";
        } else {
            return "Air defense unprepared";
        }
    }
}

class Warships implements can_attack
{
    protected $name, $speed, $fuel, $cannon;

    public function set_name($name)
    {
        $this->name = $name;
        return $this;
    }

    public function get_name()
    {
        return $this->name;
    }
    public function set_speed($speed)
    {
        $this->speed = $speed;
        return $this;
    }

    public function get_speed()
    {
        return $this->speed;
    }

    public function set_fuel($fuel)
    {
        $this->fuel = $fuel;
        return $this;
    }

    public function get_fuel()
    {
        return $this->fuel;
    }

    public function set_cannon(Cannon $cannon)
    {
        $this->cannon = $cannon;
        return $this;
    }
    public function get_info(){
        return '<br>' ."Ship Name: " . $this->get_name() . '<br>' . "Ship speed: " . $this->get_speed() . '<br>' . "Fuel: " . $this->get_fuel();
    }
    public function prepare_cannon(){
        if ($this->cannon instanceof Cannon) {
            return  $this->cannon->prepare();
        } else {
            return "Cannon not installed";
        }
    }
    public function shoot(){
        if ($this->cannon instanceof Cannon) {
            return  $this->cannon->shoot();
        } else {
            return "Nothing to shoot from, no cannon installed";
        }
    }
}

class Cruiser extends Warships
{
    protected $caliber, $ammunition;

    public function set_caliber($caliber)
    {
        $this->caliber = $caliber;
        return $this;
    }
    public function get_caliber()
    {
        return $this->caliber;
    }

    public function set_ammunition($ammunition)
    {
        $this->ammunition = $ammunition;
        return $this;
    }
    public function get_ammunition()
    {
        return $this->ammunition;
    }
public function get_info()
{
    return parent::get_info() . '<br>' ."Cannon caliber: " . $this->get_caliber() . '<br>' . "Ammunition type: " . $this->get_ammunition() .'<br>';
}
}

class Aircraft_carrier extends Warships
{
    protected $squadron;

    public function set_squadron($squadron)
    {
        $this->squadron = $squadron;
        return $this;
    }
    public function get_squadron()
    {
        return $this->squadron;
    }
    public function get_info()
    {
        return parent::get_info() .'<br>' ."Squadron: " . $this->get_squadron() . '<br>';
    }
}

$trunk = new Cannonry();
$storm = new Air_defense();
$midway = new Aircraft_carrier();
$midway
    ->set_name("Midway")
    ->set_fuel("Diesel")
    ->set_speed("30 knots")
    ->set_squadron("Stormtroopers")
    ->set_cannon($storm);
$alaska = new Cruiser();
$alaska
    ->set_name("Alaska")
    ->set_speed("38 knots")
    ->set_fuel("Diesel")
    ->set_caliber("216mm")
    ->set_ammunition("High-explosive")
    ->set_cannon($trunk);

echo $midway->get_info() . '<br>';
echo $midway->prepare_cannon() .'<br>';
echo $midway->shoot() . '<br>';
echo $alaska->get_info() . '<br>';
echo $alaska->prepare_cannon() .'<br>';
echo $alaska->shoot();
