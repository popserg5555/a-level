<?php

class Animal
{
    private $size, $class, $skin;

    public function set_size($size) {
        $this->size = $size;
        return $this;
    }
    public function set_class($class) {
        $this->class = $class;
        return $this;
    }
    public function set_skin($skin) {
        $this->skin = $skin;
        return $this;
    }

    public function get_size()
    {
        return $this->size;
    }
    public function get_class()
    {
        return $this->class;
    }
    public function get_skin()
    {
        return $this->skin;
    }

    public function get_info()
    {
        return  " size: " . $this->get_size() . " class: " . $this->get_class() . " skin: " . $this->get_skin();
    }
}
class Mammal extends Animal {
    private $paws, $habitat, $typeOfFood;

    public function set_paws($paws) {
        $this->paws = $paws;
        return $this;
    }
    public function set_habitat($habitat) {
        $this->habitat = $habitat;
        return $this;
    }
    public function set_typeOfFood($typeOfFood) {
        $this->typeOfFood = $typeOfFood;
        return $this;
    }
    public function get_paws()
    {
        return $this->paws;
    }
    public function get_habitat()
    {
        return $this->habitat;
    }
    public function get_typeOfFood()
    {
        return $this->typeOfFood;
    }
    public function get_info()
    {
        return parent::get_info() . " paws: " . $this->get_paws() . " habitat: " . $this->get_habitat() . " type of food: " . $this->get_typeOfFood();
    }
}
class Cat extends Mammal {
    private $wool, $ears, $color;

    public function set_wool($wool) {
        $this->wool = $wool;
        return $this;
    }
    public function set_ears($ears) {
        $this->ears = $ears;
        return $this;
    }
    public function set_color($color) {
        $this->color = $color;
        return $this;
    }
    public function get_wool()
    {
        return $this->wool;
    }
    public function get_ears()
    {
        return $this->ears;
    }
    public function get_color()
    {
        return $this->color;
    }
    public function get_info()
    {
        return parent::get_info() . " wool: " . $this->get_wool() . " ears: " . $this->get_ears() . " color: " . $this->get_color();
    }

}

$chordates = new Animal();
$chordates
    ->set_size("Medium")
    ->set_class("Mammals")
    ->set_skin("Woolen");

$predator = new Mammal();
$predator
    ->set_size("Medium")
    ->set_class("Mammals")
    ->set_skin("Woolen")
    ->set_paws("Four-legged")
    ->set_habitat("Land")
    ->set_typeOfFood("Mixed");

$british = new Cat();
$british
    ->set_size("Medium")
    ->set_class("Mammals")
    ->set_skin("Woolen")
    ->set_paws("Four-legged")
    ->set_habitat("Land")
    ->set_typeOfFood("Mixed")
    ->set_wool("Shorthaired")
    ->set_ears("Lop-eared")
    ->set_color("Gray");

echo $chordates->get_info() ."\n";
echo $predator->get_info() ."\n";
echo $british->get_info() ."\n";
