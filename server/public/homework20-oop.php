<?php


class DB
{
    private static $obj;

    private function __clone()
    {
        return false;
    }

    private function __wakeup()
    {
        return false;
    }

    private function __construct()
    {
        try {
            $this->con = new PDO (
                'mysql:host=a_level_nix_mysql;dbname=books', 'root', 'cbece_gead-cebfa');
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
            die();
        }
    }

    public static function createObject()
    {
        if (!is_object(self::$obj)) {
            self::$obj = new DB();
        }
        return self::$obj;
    }
}

$db = DB::createObject();

class Book
{
    private $db;

    public function __construct(DB $db)
    {
        $this->db = $db->con;
    }

    public function addBook($title, $genre_id)
    {
        $add = $this->db->query("INSERT INTO books (title,genre_id) values('$title', '$genre_id')");
        if ($add) {
            return true;
        } else {
            return false;
        }
    }

    public function showTable()
    {
        return $this->db->query("SELECT * FROM books")->fetchAll();

    }

    public function getBooksByGenreId($genre_id)
    {
        return $this->db->query("SELECT * FROM books WHERE genre_id='$genre_id'")->fetchAll();
    }

    public function deleteById($id)
    {
        $this->db->query("DELETE FROM books WHERE id = '$id'");
    }

    public function updateById($id, $title, $genre_id)
    {
        $this->db->query("UPDATE books SET title = '$title', genre_id = '$genre_id' where id = '$id'");
    }
}

$book = new Book($db);
$book->addBook("Bob and Joe", "2");
echo '<pre>';
print_r($book->showTable());
echo '</pre>';
echo '<pre>';
print_r($book->getBooksByGenreId('2'));
echo '</pre>';
$book->deleteById('27');
$book->updateById('49', 'George', '1');
$db = null;

