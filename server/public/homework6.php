<?php
require_once 'function.php';

//Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand).
// Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные индексы.
// После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.

$res = getRandomNumbers(100);
echo '<pre>';
// phpcs:disable
print_r($res);
// phpcs:enable
echo '</pre>';
//------------------------------------------------------------------------------------
$multiEven = calcMultiplicationEven($res);
echo $multiEven . '<br>';
//------------------------------------------------------------------------
calcMultiplicationOdd($res);

//Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.

$summa = calcSum(1, 5);
$multi = calcMulti(2, 4);
$square = calcSquare(2, 4);
info($summa);
info($multi);
info($square);

//Даны три числа. Найдите их среднее арифметическое.


$average = calcAverage(2, 4, 6);
info($average);

//Дано число. Увеличьте его на 30%, на 120%.


$increase_30 = calcIncreaseX30(50, 30);
$increase_120 = calcIncreaseX120(50, 120);
info($increase_30);
info($increase_120);


/*Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия),
вводит количество дней для отдыха и указывает, есть ли у него скидка (чекбокс).
Вывести стоимость отдыха, которая вычисляется как произведение   количества дней на 400.
Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия.
И далее это число уменьшается на 5%, если указана скидка. */

 echo '<form action="'.$_SERVER['SCRIPT_NAME'].'" method="POST">
        <select name="country">
            <option value="Turkey">Турция</option>
            <option value="Egypt">Египет</option>
            <option value="Italy">Италия</option>
        </select>
        <br>
        <input type="number" name="days" placeholder="Введите кол-во дней"><br>
        <p>У Вас есть скидка?</p>
        <input type="checkbox" name="discount" value="true">Да<br>
        <input type="checkbox" name="discount" value="false">Нет<br>
        <input type="submit" name="calculation" value="Рассчитать">
    </form>';

if ($_REQUEST['calculation']) {
    $country = $_REQUEST['country'];
    $days = $_REQUEST['days'];
    $discount = $_REQUEST['discount'];

    $price = calcPrice($days);
    $select = calcDiscount($country, $price);
    $calculation = calcTotal($select, $discount);
    echo 'Цена :' . $price . '<br>';
    echo 'Цена выбранной страны: ' . $select . '<br>';
    echo 'Итоговая цена :' . $calculation . '<br>';
}


//Пользователь вводит свой имя, пароль, email.
// Если вся информация указана, то показать эти данные после фразы 'Регистрация прошла успешно',
// иначе сообщить какое из полей оказалось не заполненным.

echo '<form action="'.$_SERVER['SCRIPT_NAME'].'" method="POST">
        <input type="text" name="user_name" placeholder="Введите имя :">
        <input type="password" name="user_password" placeholder="Введите пароль :">
        <input type="email" name="user_email" placeholder="Введите e-mail :">
        <input type="submit" name="send_info" value="Зарегистрироваться">
    </form>';


$user_name = $_REQUEST['user_name'];
$user_password = $_REQUEST['user_password'];
$user_email = $_REQUEST['user_email'];
if (isset($_REQUEST['send_info'])) {
    showInfo($user_name, $user_password, $user_email);
}

//Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь на форме.
// Если n некорректно, вывести фразу "Bad n".

  echo '<form action="'.$_SERVER['SCRIPT_NAME'].'" method="POST">
        <input type="text" name="num" placeholder="Введите число">
        <input type="submit" name="Send" value="Отправить">
    </form>';

if (isset($_REQUEST['Send'])) {
    $n = $_REQUEST['num'];
    showN($n);
}

//Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля

echo '<pre>';
// phpcs:disable
print_r(createMyArray(10));
// phpcs:enable
echo '</pre>';


//Определите, есть ли в массиве повторяющиеся элементы.
$b = [1, 5, 43, 1, 9, 7, 6, 10, 45, 76];
echo '<pre>';
// phpcs:disable
print_r($b);
// phpcs:enable
echo '</pre>';
$c = checkDoubleInArray($b);
if ($c) {
    echo 'Есть повторение' . '<br>';
} else {
    echo 'Повторений нет' . '<br>';
}


//Найти минимальное и максимальное среди 3 чисел

$res = getMin(6, 6, 2);
info($res);
$res = getMax(4, 6, 3);
info($res);

//Найти площадь

$res = getSquareByParams(4, 5);
info($res);

//Теорема Пифагора

$c = getPifagor(3, 4);
info($c);

//Найти периметр

$c = getPerimeter(3, 4);
info($c);

//Найти дискриминант

$D = getDiscriminant(2, 8, 4);
info($D);

//Создать только четные числа до 100
$arrEven = getEvenArray(100);
echo '<pre>';
// phpcs:disable
print_r($arrEven);
// phpcs:enable
echo '</pre>';

//Создатьне четные числа до 100

$arrOdd = getOddArray(100);
echo '<pre>';
// phpcs:disable
print_r($arrOdd);
// phpcs:enable
echo '</pre>';

//Создать функцию по нахождению числа в степени

$d = calcDegree(3, 3);
info($d);

//написать функцию сортировки. Функция принимает массив случайных чисел и сортирует их по порядку.
// По дефолту функция сортирует в порядке возрастания.
// Но если передать в сторой параметр то функция будет сортировать по убыванию.
//sort(arr)
//sort(arr, 'asc')
//sort(arr, 'desc')

$array = [1,5,8,3,65,7,32,88,101];
echo '<pre>';
// phpcs:disable
print_r(sortArray($array, 'asc'));
// phpcs:enable
echo '</pre>';
echo '<pre>';
// phpcs:disable
print_r(sortArray($array, 'desc'));
// phpcs:enable
echo '</pre>';
echo '<pre>';
// phpcs:disable
print_r(sortArray($array));
// phpcs:enable
echo '</pre>';


//написать функцию поиска в массиве. функция будет принимать два параметра.
// Первый массив, второй поисковое число. search(arr, find)
$a = [4,5,6,2,8,0,12];
$findvalue = isIssetInArray($a, 10);
echo '<pre>';
// phpcs:disable
print_r($a);
// phpcs:enable
echo '</pre>';
if ($findvalue) {
    echo 'Number detected';
} else {
    echo 'Number dont find';
}
