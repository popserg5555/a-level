<?php

//Создайте функцию, которая подсчитывает количество директорий в массиве.
//Примеры:
//1  == solution(["C:/Projects/something.txt","file.exe"])
//0  == solution(["brain-games.exe","gendiff.sh","task-manager.rb"])
//3  == solution(["C:/Users/JohnDoe/Music/Beethoven_5.mp3","/usr/bin","/var/www/myprojectt"])

$a = ['C:/Projects/something.txt', 'file.exe'];
/**
 * @param array $a
 * @return integer
 * function for calculating directories in an array
 */
function solution(array $a): int
{
    $counter = 0;
    foreach ($a as $item) {
        $path_array = explode('/', $item);
        foreach ($path_array as $value) {
            if (strripos($value, ':') === false && strripos($value, '.') === false && $value != '') {
                $counter++;
            }
        }
        return $counter; //считает количество директорий в первом элементе массива
    }

    return 0;
//    return $counter; ( считает количество директорий во всем массиве)
}

echo solution($a) . '<br>';

$a = ['brain-games.exe', 'gendiff.sh', 'task-manager.rb'];
echo solution($a) . '<br>';
$a = ['C:/Users/JohnDoe/Music/Beethoven_5.mp3', '/usr/bin', '/var/www/myprojectt'];
echo solution($a) . '<br>';


//Создайте функцию, которая принимает массив из трех элементов представляющих собой результат запуска слот-машины из казино.
// Проверьте, является ли комбинация элементов удачной (все элементы равны).
//Примеры:
//true  == solution(["9919","9919","9919"])
//false  == solution(["abc","abc","abb"])
//true  == solution(["@","@","@"])
$a = ['9919', '9919', '9919'];
$succes = function (array $a): bool {
    if ($a[0] == $a[1] && $a[1] == $a[2] && $a[0] = $a[2]) {
        return true;
    } else {
        return false;
    }
};
echo $succes($a) ? 'true <br>' : 'false <br>';

$a = ['abc', 'abc', 'abb'];
echo $succes($a) ? 'true <br>' : 'false <br>';

$a = ['@', '@', '@'];
echo $succes($a) ? 'true <br>' : 'false <br>';

//----------------------------------------------------------------------------------

//Сэму и Фродо надо держаться вместе. Проверьте, нет ли между ними других персонажей.
//Примеры:
//true  == solution(["Sam","Frodo","Troll","Balrog","Human"])
//false  == solution(["Orc","Frodo","Treant","Saruman","Sam"])
//true  == solution(["Orc","Sam","Frodo","Gandalf","Legolas"])

$a = ['Sam', 'Frodo', 'Troll', 'Balrog', 'Human'];

$between = function (array $a): bool {
    $count = count($a);
    for ($i = 0; $i < $count; $i++) {
        if (($a[$i] == 'Sam' && $a[$i + 1] == 'Frodo') || ($a[$i] == 'Frodo' && $a[$i + 1] == 'Sam')) {
            return true;
        }
    }
    return false;
};
echo $between($a) ? 'true <br>' : 'false <br>';

$a = ['Orc', 'Frodo', 'Treant', 'Saruman', 'Sam'];
echo $between($a) ? 'true <br>' : 'false <br>';

$a = ['Orc', 'Sam', 'Frodo', 'Gandalf', 'Legolas'];
echo $between($a) ? 'true <br>' : 'false <br>';

//---------------------------------------------------------------------------------

//Найдите второе наибольшее число в массиве.
//Примеры:
//2  == solution([1,2,3])
//1  == solution([1,-2,3])
//0  == solution([0,0,10])
//-2  == solution([-1,-2,-3])


$arr = [1, 2, 3];
rsort($arr);
echo $arr[1];

$arr = [1, -2, 3];
rsort($arr);
echo $arr[1];

$arr = [0, 0, 10];
rsort($arr);
echo $arr[1];

$arr = [-1, -2, -3];
rsort($arr);
echo $arr[1] . '<br>';

//----------------------------------------------------------------------------------------------------
//Дан массив строк, создайте функцию, которая создает новый массив, содержащий строки, длины которых соответствуют наидлиннейшей строке.
//Примеры:
//["programms"]  == solution(["in","Soviet","Russia","frontend","programms","you"])
//["clojure","greater"]  == solution(["using","clojure","makes","your","life","greater"])
//["a","b","c","d"]  == solution(["a","b","c","d"])

$array = array('in', 'Soviet', 'Russia', 'frontend', 'programms', 'you');
/**
 * @param array $array
 * @return array $a
 * function to get the array with the most characters
 */
function getArrayWithMostCharacters(array $array): array
{
    $a = [];
    $maxChars = 0;
    foreach ($array as $item) {
        if ($maxChars < strlen($item)) {
            $maxChars = strlen($item);
        }
    }
    foreach ($array as $item) {
        if (strlen($item) == $maxChars) {
            $a[] = $item;
        }
    }
    return $a;
}

echo '<pre>';
// phpcs:disable
print_r(getArrayWithMostCharacters($array));
// phpcs:enable
echo '</pre>';


$array = array('using', 'clojure', 'makes', 'your', 'life', 'greater');
echo '<pre>';
// phpcs:disable
print_r(getArrayWithMostCharacters($array));
// phpcs:enable
echo '</pre>';

$array = array('a', 'b', 'c', 'd');
echo '<pre>';
// phpcs:disable
print_r(getArrayWithMostCharacters($array));
// phpcs:enable
echo '</pre>';


//Рассчитайте финальную оценку студента по пяти предметам.
// Если средняя оценка больше 90, то итоговая A.
// Если средняя оценка больше 80, то итоговая B.
// Если средняя оценка больше 70, то итоговая оценка C.
// Если средняя оценка больше 60, то итоговая оценка D.
// В остальных случаях итоговая оценка F.
//Примеры:
//"Grade: A"  == solution([90,91,99,93,100])
//"Grade: B"  == solution([92,77,85,84,84])
//"Grade: C"  == solution([70,72,78,72,70])
//"Grade: D"  == solution([60,61,62,63,70])
//"Grade: F"  == solution([50,42,20,31,0])
//"Grade: F"  == solution([10,9,2,3,5])

$a = [90, 91, 99, 93, 100];
/**
 * @param array $a
 * @return string
 * the function counts the average
 */
function calcAverage(array $a): string
{
    $res = ($a[0] + $a[1] + $a[2] + $a[3] + $a[4]) / 5;
    echo $res . '<br>';
    if ($res > 90) {
        return 'Grade A' . '<br>';
    } elseif ($res > 80 && $res < 90) {
        return 'Grade B' . '<br>';
    } elseif ($res > 70 && $res < 80) {
        return 'Grade C' . '<br>';
    } elseif ($res > 60 && $res < 70) {
        return 'Grade D' . '<br>';
    } else {
        return 'Grade F' . '<br>';
    }
}

echo calcAverage($a);

$a = [92, 77, 85, 84, 84];
echo calcAverage($a);
$a = [70, 72, 78, 72, 70];
echo calcAverage($a);
$a = [50, 42, 20, 31, 0];
echo calcAverage($a);
$a = [10, 9, 2, 3, 5];
echo calcAverage($a);
// ---------------------------------------------------------------------------------------
//Создайте функцию которая принимает целое число и возвращает строку с названием фигуры, состоящий из переданного количество сторон.
//Примеры:
//"circle"  == solution(1)
//"semi-circle"  == solution(2)
//"triangle"  == solution(3)
//"square"  == solution(4)
//"pentagon"  == solution(5)
//"hexagon"  == solution(6)
//"heptagon"  == solution(7)
//"octagon"  == solution(8)
//"nonagon"  == solution(9)
//"decagon"  == solution(10)


$figura = function (int $n): string {
    if ($n == 1) {
        return 'circle';
    } else if ($n == 2) {
        return 'semi-circle';
    } else if ($n == 3) {
        return 'triangle';
    } else if ($n == 4) {
        return 'square';
    } else if ($n == 5) {
        return 'pentagon';
    } else if ($n == 6) {
        return 'hexagon';
    } else if ($n == 7) {
        return 'heptagon';
    } else if ($n == 8) {
        return 'octagon';
    } else if ($n == 9) {
        return 'nonagon';
    } else if ($n == 10) {
        return 'decagon';
    }

    return '';
};
echo $figura(7) . '<br>';

//-----------------------------------------------------------------------------
//Фермер просит вас посчитать сколько ног у всех его животных.
// Фермер разводит три вида: курицы = 2 ноги коровы = 4 ноги свиньи = 4 ноги
// Фермер посчитал своих животных и говорит вам, сколько их каждого вида.
// Вы должны написать функцию, которая возвращает общее число ног всех животных.
//Примеры:
//36  == solution(2, 3, 5)
//22  == solution(1, 2, 3)
//50  == solution(5, 2, 8)

/**
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return integer
 */
function calcLeg($a, $b, $c): int
{
    $k = 2;
    $s = 4;
    $result = ($a * $k) + ($b * $s) + ($c * $s);

    return $result;
}

echo calcLeg(2, 3, 5) . '<br>';
echo calcLeg(1, 2, 3) . '<br>';
echo calcLeg(5, 2, 8) . '<br>';

//--------------------------------------------------------------------------
//Создайте функцию, которая трансформирует массив слов в массив длин этих слов.
//Примеры:
//[5,5]  == solution(["hello","world"])
//[4,4,4,7]  == solution(["some","test","data","strings"])
//[7]  == solution(["clojure"])
$k = ['hello', 'world'];
/**
 * @param array $array
 * @return array
 * function counts the length of the elements in the array
 */
function getArrayWithCountCharsItems(array $array): array
{
    $a = [];
    foreach ($array as $item) {
        $a[] = strlen($item);
    }
    return $a;
}

echo '<pre>';
// phpcs:disable
print_r(getArrayWithCountCharsItems($k));
// phpcs:enable
echo '</pre>';
$k = ['some', 'test', 'data', 'strings'];
echo '<pre>';
// phpcs:disable
print_r(getArrayWithCountCharsItems($k));
// phpcs:enable
echo '</pre>';

$k = ['clojure'];
echo '<pre>';
// phpcs:disable
print_r(getArrayWithCountCharsItems($k));
// phpcs:enable
echo '</pre>';


//Создайте функцию, которая принимает слово на английском языке и проверяет, во множественном ли числе находится слово.
// Проверяйте самый простой вариант.
//Примеры:
//false  == solution("fork")
//true  == solution("forks")
//false  == solution("clojure")
//true  == solution("bytes")
//false  == solution("test")
/**
 * @param string $word
 * @return boolean
 * function checks a word in the plural or not
 */
function checkWordIsPlural($word): bool
{
    $lengthString = strlen($word);
    if ($word[$lengthString - 1] == 's') {
        return true;
    }
    return false;
}

echo checkWordIsPlural('fork') ? 'true <br>' : 'false <br>';
echo checkWordIsPlural('forks') ? 'true <br>' : 'false <br>';
echo checkWordIsPlural('clojure') ? 'true <br>' : 'false <br>';
echo checkWordIsPlural('bytes') ? 'true <br>' : 'false <br>';
echo checkWordIsPlural('test') ? 'true <br>' : 'false <br>';
