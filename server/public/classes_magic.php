<?php


class Dogs
{

    protected $properties = ['breed', 'name', 'color', 'age'];
    protected $breed, $name, $color, $age;


    public function __construct($breed, $name, $color, $age)
    {
        $this->breed = $breed;
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }

    public function __invoke()
    {
        return "Вы вызвали меня как функцию" . '<br>';
    }

    protected function getMessage()
    {
        return "Собаки очень отзывчивые" . '<br>';
    }

    public function __call($method, $parametrs)
    {
        return "Вызов call" . '<br>';
    }

    public function __set($prop, $val)
    {
        if (in_array($prop, $this->properties)) {
            $this->$prop = $val;
            return $this;
        } else return false;
    }

    public function __get($prop)
    {
        if (in_array($prop, $this->properties)) {
            return $this->$prop;
        } else return false;
    }
    public function __isset($name){
        echo "Установлено ли '$name' ?" . '<br>';
        return isset($this->$name);
    }
    public function __unset($breed){
        unset($this->breed);
    }

    public function __toString()
    {

        return implode(", ",
                array_map([$this, '__get'], $this->properties)
            ) . "\n";

        $result = [];
        foreach ($this->properties as $prop) {
            $result[] = $this->__get($prop);
        }
        unset($this->$name);
        return implode(", ", $result) . "\n";
    }

    public function __clone()
    {
        $this->breed = 'Shepherd';
        $this->age = 10;
    }

    function __destruct()
    {
        echo "Вы меня удалили" . '<br>';
    }
}


$labrador = new Dogs("Labrador", "Charly", "Sandy", 7);
echo $labrador . '<br>';

var_dump(isset($labrador->name));

unset($labrador->breed);
echo '<br>';

var_dump(isset($labrador->breed));
echo '<br>';

$shepherd = clone $labrador;
echo $shepherd . '<br>';

echo $labrador();

echo $labrador->getMessage();


