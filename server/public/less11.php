<?php
/**
 * @param array $arr
 * @return boolean
 * function that outputs an array
 */
function dd(array $arr): bool
{
    echo '<pre>';
    // phpcs:disable
    print_r($arr);
    // phpcs:enable
    echo '</pre>';
    return true;
}

/**
 * @param  string $val
 * @param  string $result
 * @return string
 */
function expect($val, $result): string
{
    $res = ($val == $result) ? 'Success' : 'Error';
    return $res;
}
