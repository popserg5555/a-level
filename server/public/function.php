<?php
/**
 * @param  integer $word
 * @return void
 * the function outputs a numeric value
 */
function info($word): void
{
    echo $word .'<br>';
}

//-----------------------------------------------------
//Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand).
// Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные индексы.
// После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.
/**
 * @param array $arr
 * @return array $a
 * the function generates an array of random numbers
 */
function getRandomNumbers(int $arr): array
{
    $a = [];
    for ($i = 0; $i <= $arr; $i++) {
        $a[] = rand(1, 100);
    }
    return $a;
}

//-----------------------------------------------------
/**
 * @param  array $res
 * @return float
 * the function outputs even numbers
 */
function calcMultiplicationEven(array $res): float
{
    $x = 1;
    $count = count($res);
    for ($i = 0; $i <= $count; $i += 2) {
        $x *= $res[$i];
    }
    return $x;
}

//-----------------------------------------------------
/**
 * @param array $res
 * @return string
 * the function outputs odd numbers
 */
function calcMultiplicationOdd(array $res): string
{
    foreach ($res as $key => $value) {
        if ($value > 0 && $key % 2 == 1) {
            return $key . '=>' . $value . '<br>';
        }
    }

    return '';
}

//-----------------------------------------------------
//Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.
/**
 * @param integer $x
 * @param integer $y
 * @return integer
 * the function calculates the sum of numbers
 */
function calcSum($x, $y): int
{
    return $x + $y;
}

/**
 * @param integer $x
 * @param integer $y
 * @return float
 * the function counts the multiplication of numbers
 */
function calcMulti($x, $y): float
{
    return $x * $y;
}

/**
 * @param integer $x
 * @param integer $y
 * @return float
 * the function calculates the area
 */
function calcSquare($x, $y): float
{
    return ($x * $x) + ($y * $y);
}

//-----------------------------------------------------
//Даны три числа. Найдите их среднее арифметическое.
/**
 * @param integer $x
 * @param integer $y
 * @param integer $z
 * @return float
 * the function counts the average
 */
function calcAverage($x, $y, $z): float
{
    return ($x + $y + $z) / 3;
}

//-----------------------------------------------------
//Дано число. Увеличьте его на 30%, на 120%.
/**
 * @param  integer $x
 * @param  float   $percent
 * @return float
 * the function counts an increase in a number by a percentage
 */
function calcIncreaseX30($x, $percent): float
{
    return ($x * ($percent / 100 + 1));
}

/**
 * @param integer $x
 * @param float   $percent
 * @return float
 * the function counts an increase in a number by a percentage
 */
function calcIncreaseX120($x, $percent): float
{
    return ($x * ($percent / 100 + 1));
}

//-----------------------------------------------------
/*Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия),
вводит количество дней для отдыха и указывает, есть ли у него скидка (чекбокс).
Вывести стоимость отдыха, которая вычисляется как произведение   количества дней на 400.
Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия.
И далее это число уменьшается на 5%, если указана скидка. */
/**
 * @param integer $days
 * @return integer
 * the function calculates the price depending on the number of days
 */
function calcPrice($days): int
{
    return $days * 400;
}

//---------------------------------------------------------
/**
 * @param string  $country
 * @param integer $price
 * @return integer
 * the function considers the discount
 */
function calcDiscount($country, $price): int
{
    if ($country == 'Egypt') {
        return $price * (10 / 100 + 1);
    } elseif ($country == 'Italy') {
        return $price * (12 / 100 + 1);
    }
    return $price;
}

//---------------------------------------------------------
/**
 * @param integer $price
 * @param integer $discount
 * @return float
 * the function calculates the total
 */
function calcTotal($price, $discount): float
{
    if ($discount == 'true') {
        return $price * 0.95;
    }
    return $price;
}

//---------------------------------------------------------
//Пользователь вводит свой имя, пароль, email.
// Если вся информация указана, то показать эти данные после фразы 'Регистрация прошла успешно',
// иначе сообщить какое из полей оказалось не заполненным.
/**
 * @param string $user_name
 * @param string $user_password
 * @param string $user_email
 * @return void
 * the function processes the input field information
 */
function showInfo($user_name, $user_password, $user_email): void
{
    if ($user_name && $user_password && $user_email) {
        echo 'Регистрация прошла успешно' . '<br>' . $user_name . '<br>' . $user_password . '<br>' . $user_email;
    } elseif ($user_name && $user_password) {
        echo 'Вы не заполнили Email!';
    } elseif ($user_email && $user_password) {
        echo 'Вы не заполнили Имя!';
    } elseif ($user_name && $user_email) {
        echo 'Вы не заполнили Пароль';
    } elseif ($user_name) {
        echo 'Вы не заполнили Пароль и E-mail';
    } elseif ($user_password) {
        echo 'Вы не заполнили Имя и E-mail';
    } elseif ($user_email) {
        echo 'Вы не заполнили Имя и Пароль';
    } else {
        echo 'Вы должны заполнить поля!';
    }
}

//---------------------------------------------------------
//Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь на форме.
// Если n некорректно, вывести фразу "Bad n".
/**
 * @param string $n
 * @return void
 * the function shows the phrase n number of times
 */
function showN($n): void
{
    if (ctype_digit($n) && $n != 0) {
        while ($n != 0) {
            echo 'Silence is golden' . '<br>';
            $n--;
        }
    } else {
        echo 'Bad n';
    }
}

//---------------------------------------------------------
//Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля
/**
 * @param integer $a
 * @return array
 * the function fills the array with numbers from 0 to 1
 */
function createMyArray($a): array
{
    $my_array = [];
    for ($i = 0; $i <= $a; $i++) {
        $my_array[] = ($i % 2 == 0) ? 0 : 1;
    }
    return $my_array;
}

//---------------------------------------------------------
//Определите, есть ли в массиве повторяющиеся элементы.
/**
 * @param array $a
 * @return boolean
 * the function determines if there are duplicate elements in the array
 */
function checkDoubleInArray(array $a): bool
{
    foreach ($a as $item_key => $item) {
        foreach ($a as $value_key => $value) {
            if ($item == $value && $item_key != $value_key) {
                return true;
            }
        }
    }
    return false;
}

//--------------------------------------------------------------
//Найти минимальное и максимальное среди 3 чисел
/**
 * @param integer $x
 * @param integer $y
 * @param integer $z
 * @return mixed|void
 * the function looks for the minimum value among the numbers
 */
function getMin($x, $y, $z): int
{
    if ($x < $y && $x < $z) {
        return $x;
    } elseif ($y < $x && $y < $z) {
        return $y;
    } elseif ($z < $x && $z < $y) {
        return $z;
    }
}

/**
 * @param integer $x
 * @param integer $y
 * @param integer $z
 * @return mixed|void
 * the function looks for the maximum value among the numbers
 */
function getMax($x, $y, $z): int
{
    if ($x > $y && $x > $z) {
        return $x;
    } elseif ($y > $x && $y > $z) {
        return $y;
    } elseif ($z > $x && $z > $y) {
        return $z;
    }
}

//--------------------------------------------------------------
//Найти площадь
/**
 * @param integer $x
 * @param integer $y
 * @return float
 * the function calculates the area
 */
function getSquareByParams($x, $y): float
{
    return $x * $y;
}

//--------------------------------------------------------------
//Теорема Пифагора
/**
 * @param integer $a
 * @param integer $b
 * @return float
 * function calculates the Pythagorean theorem
 */
function getPifagor($a, $b): float
{
    return sqrt(($a * $a) + ($b * $b));
}

//--------------------------------------------------------------
//Найти периметр
/**
 * @param integer $a
 * @param integer $b
 * @return float
 * function calculates the perimeter
 */
function getPerimeter($a, $b): float
{
    return ($a + $b) * 2;
}

//--------------------------------------------------------------
//Найти дискриминант
/**
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return float
 * the function calculates the discriminant
 */
function getDiscriminant($a, $b, $c): float
{
    return $b * $b - 4 * $a * $c;
}

//--------------------------------------------------------------
//Создать только четные числа до 100
/**
 * @param integer $count
 * @return array
 * the function only creates even numbers up to 100
 */
function getEvenArray($count): array
{
    $even = [];
    for ($i = 0; $i <= $count; $i += 2) {
        if ($i != 0) {
            $even[] = $i;
        }
    }
    return $even;
}


//--------------------------------------------------------------
//Создать нечетные числа до 100
/**
 * @param integer $count
 * @return array
 * the function only creates odd numbers up to 100
 */
function getOddArray($count): array
{
    $odd = [];
    for ($i = 1; $i <= $count; $i += 2) {
        if ($i != 0) {
            $odd[] = $i;
        }
    }
    return $odd;
}

//--------------------------------------------------------------
//Создать функцию по нахождению числа в степени
/**
 * @param integer $x
 * @param integer $degree
 * @return mixed
 * the function finds a number to the power
 */
function calcDegree($x, $degree): int
{
    return $x ** $degree;
}

//--------------------------------------------------------------
//написать функцию сортировки. Функция принимает массив случайных чисел и сортирует их по порядку.
// По дефолту функция сортирует в порядке возрастания.
// Но если передать в сторой параметр то функция будет сортировать по убыванию.
//sort(arr)
//sort(arr, 'asc')
//sort(arr, 'desc')
/**
 * @param array $array
 * @return array
 * descending sort function
 */
function sortWaning(array $array): array
{
    $numbers = $array;
    $result = [];
    foreach ($numbers as $key_numb => $numb) {
        $maxNumber = $numbers[$key_numb];
        $maxNumberKey = $key_numb;
        foreach ($numbers as $key => $value) {
            if ($value > $maxNumber) {
                $maxNumber = $value;
                $maxNumberKey = $key;
            }
        }
        $result[] = $numbers[$maxNumberKey];
        unset($numbers[$maxNumberKey]);
    }
    return $result;
}
/**
 * @param array $array
 * @return array
 * ascending sort function
 */
function sortIncrease(array $array): array
{
    $numbers = $array;
    foreach ($numbers as $key_numb => $numb) {
        $minNumber = $numbers[$key_numb];
        $minNumberKey = $key_numb;
        foreach ($numbers as $key => $value) {
            if ($value < $minNumber) {
                $minNumber = $value;
                $minNumberKey = $key;
            }
        }
        $result[] = $numbers[$minNumberKey];
        unset($numbers[$minNumberKey]);
    }
    return $result;
}

/**
 * @param array  $array
 * @param string $sort
 * @return array
 */
function sortArray(array $array, $sort = 'asc'): array
{
    switch ($sort) {
        case 'asc' :
            return sortIncrease($array);
        case 'desc' :
            return sortWaning($array);
        default :
            return sortIncrease($array);
    }
}

//------------------------------------------------------
//написать функцию поиска в массиве. функция будет принимать два параметра.
// Первый массив, второй поисковое число. search(arr, find)
/**
 * @param array   $array
 * @param integer $find
 * @return boolean
 * search function for a number in an array
 */
function isIssetInArray(array $array, $find): int
{
    foreach ($array as $item) {
        if ($item == $find) {
            return true;

        }
    }
    return false;
}
