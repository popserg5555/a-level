<?php
/**
 * @param array $item
 * @return void
 * the function outputs an array
 */
function dd(array $item): void
{
    // phpcs:disable
    echo '<pre>';
    print_r($item);
    echo '</pre>';
    // phpcs:enable
}

/**
 * @param  integer $word
 * @return void
 * the function outputs a numeric value
 */
function info($word): void
{
    echo $word .'<br>';
}

//Найти минимальное и максимальное среди 3 чисел
/**
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return void
 * the function finds the maximum and minimum number
 */
$minAndMax = function (int $a, int $b, int $c): void {
    if ($a > $b && $a > $c || $a == $b || $a == $c) {
        echo $a .'Максимальное число' . '</br>';
    } elseif ($b > $a && $b > $c || $b == $c || $b == $a) {
        echo $b .'Максимальное число' . '</br>';
    } elseif ($c > $a && $c > $b || $c == $b || $c == $a) {
        echo $c .'Максимальное число' . '</br>';
    }
    if ($a < $b && $a < $c) {
        echo $a .'Минимальное число' . '</br>';
    } elseif ($b < $a && $b < $c) {
        echo $b .'Минимальное число' . '</br>';
    } elseif ($c < $a && $c < $b) {
        echo $c .'Минимальное число' . '</br>';
    }
};
echo $minAndMax(8, 78, 78) . '</br>';

$max_function = fn($a, $b, $c) => $minAndMax($a, $b, $c);
echo $max_function(1, 5, 12) . '</br>';

$min_function = fn($a, $b, $c) => $minAndMax($a, $b, $c);
echo $min_function(3, 5, 9) . '</br>';


//Найти площадь----------------------------------------------------------------
/**
 * @param integer $a
 * @param integer $b
 * @return float
 * the function calculates the area
 */
$squareFunc = function (int $a, int $b): float {
    return $a * $b;
};
echo $squareFunc(4, 5) . '</br>';

$squareFunc1 = fn($a, $b) => $a * $b;
echo $squareFunc1(5, 6) . '</br>';


//Теорема Пифагора--------------------------------------------------------
/**
 * @param integer $a
 * @param integer $b
 * @return float
 * the function considers the Pythagorean theorem
 */
$pifagorFunc = function (int $a, int $b): float {
    return sqrt(($a * $a) + ($b * $b));
};
echo $pifagorFunc(4, 3) . '</br>';

$pifagorFunc1 = fn($a, $b) => sqrt(($a * $a) + ($b * $b));
echo $pifagorFunc1(3, 4) . '</br>';


//Найти периметр--------------------------------------------------------------------
/**
 * @param integer $a
 * @param integer $b
 * @return float
 * function counts perimeter
 */
$perimetrFunc = function (int $a, int $b): float {
    return 2 * ($a + $b);
};
echo $perimetrFunc(3, 5) . '</br>';

$perimetrFunc1 = fn($a, $b) => 2 * ($a + $b);
echo $perimetrFunc1(5, 4) . '</br>';


//Найти дискриминант--------------------------------------------------------------------
/**
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return float
 * function considers discriminant
 */
$discriminantFunc = function (int $a, int $b, int $c): float {
    return ($b * $b) - 4 * $a * $c;
};
echo $discriminantFunc(1, 6, 2) . '</br>';

$discriminantFunc1 = fn($a, $b, $c) => ($b * $b) - 4 * $a * $c;
echo $discriminantFunc1(3, 7, 2) . '</br>';

//Создать только четные числа до 100--------------------------------------
/**
 * the function creates even numbers up to a hundred
 */
$evenFunc = function () {
    for ($i = 2; $i < 100; $i += 2) {
        info($i);
    }
};
echo $evenFunc() . '</br>';


$array = [];
for ($i = 0; $i < 100; $i++) {
    $array[] = [$i];
}

$evenFunc1 = fn($array) => $i % 2 == 0;
info($i);
 echo $evenFunc1($array) .'</br>';

//Создать нечетные числа до 100-------------------------------------------
/**
 * the function creates odd numbers up to a hundred
 */
$oddFunc = function () {
    for ($i = 1; $i < 100; $i += 2) {
        info($i);
    }
};
echo $oddFunc() .'</br>';

$oddFunc1 = fn($x) => $x % 2 == 1;
echo $oddFunc1(100) .'</br>';

//Определите, есть ли в массиве повторяющиеся элементы.-------------------------------------------
/**
 * the function checks if there are duplicate elements in the array
 */
$arr = [1,5,44,55,4,57,7,8,101,14,10];
$filter = function ($arr) {
    foreach ($arr as $key => $item) {
        foreach ($arr as $value_key => $value) {
            if ($item == $value && $key != $value_key) {
                return true;
            }
        }
    }
    return false;
};

if ($filter($arr)) {
    echo 'Есть повтор';
} else {
    echo 'Повторов нет';
}
echo '<br>';
//------------------------------------------------------------------------------------
$filter_array = array_filter($arr, fn() => $filter($arr));
if ($filter_array) {
    echo 'Есть повтор';
} else {
    echo 'Повторов нет';
}

//Сортировка из прошлого задания----------------------------------------------

$array = [1,5,8,3,65,7,32,88,101];
/**
 * @param array $array
 * @return array
 * the function sorts the array in descending order
 */
function sortWaning(array $array): array
{
    $numbers = $array;
    $result = [];
    foreach ($numbers as $key_numb => $numb) {
        $maxNumber = $numbers[$key_numb];
        $maxNumberKey = $key_numb;
        foreach ($numbers as $key => $value) {
            if ($value > $maxNumber) {
                $maxNumber = $value;
                $maxNumberKey = $key;
            }
        }
        $result[] = $numbers[$maxNumberKey];
        unset($numbers[$maxNumberKey]);
    }
    return $result;
};

$array = [1,5,8,3,65,7,32,88,101];
/**
 * @param array $array
 * @return array
 * the function sorts the array in ascending order
 */
function sortIncrease(array $array): array
{
    $numbers = $array;
    foreach ($numbers as $key_numb => $numb) {
        $minNumber = $numbers[$key_numb];
        $minNumberKey = $key_numb;
        foreach ($numbers as $key => $value) {
            if ($value < $minNumber) {
                $minNumber = $value;
                $minNumberKey = $key;
            }
        }
        $result[] = $numbers[$minNumberKey];
        unset($numbers[$minNumberKey]);
    }
    return $result;
};

$array_numbers = [1,5,8,3,65,7,32,88,101];
$sortArray = function ($array, $sort = '') {
    switch ($sort) {
        case 'asc' :
            sort($array);
            break;
        case 'desc' :
            rsort($array);
            break;
        default :
            sort($array);
    }
    return $array;
};
dd($sortArray($array_numbers, 'asc'));
//-----------------------------------------------------------------
$sort = 'asc';
$sortArray = fn(&$array, $sort) => $sort == 'desc' ? rsort($array) : sort($array);
$sortArray($array_numbers, $sort);
dd($array_numbers);
